﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Pcf.Common
{
    public class PartnerManagerIdMessage
    {
        public Guid? PartnerManagerId { get; set; }
    }
}
